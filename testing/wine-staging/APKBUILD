# Contributor: Pedro Filipe <xpecex@outlook.com>
# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Martell Malone <martell@marinelayer.io>
# Contributor: Craig Andrews <candrews@integralblue.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Contributor: sewn <sewn@disroot.org>
# Maintainer: Matthias Ahouansou <matthias@ahouansou.cz>
pkgname=wine-staging
pkgver=8.20
pkgrel=0
pkgdesc="Compatibility layer for running Windows programs -- Staging patchset"
url="https://www.winehq.org"
arch="x86 x86_64"
license="LGPL-2.0-or-later"
depends="
	libxi
	libxrandr
	"
depends_dev="$pkgname perl"
makedepends="
	alsa-lib-dev
	autoconf
	automake
	bison
	bash
	python3
	cups-dev
	dbus-dev
	flex-dev
	fontconfig-dev
	freetype-dev
	gnutls-dev
	gstreamer-dev
	gst-plugins-base-dev
	krb5-dev
	libgphoto2-dev
	libpcap-dev
	libusb-dev
	libxcomposite-dev
	libxcursor-dev
	libxi-dev
	libxinerama-dev
	libxrandr-dev
	libxrender-dev
	mesa-dev
	mingw-w64-gcc
	ncurses-dev
	opencl-dev
	pcsc-lite-dev
	pulseaudio-dev
	sane-dev
	sdl2-dev
	udisks2-dev
	v4l-utils-dev
	vulkan-loader-dev
	wayland-dev
	"
subpackages="$pkgname-dev $pkgname-doc"
checkdepends="xvfb-run"
source="https://dl.winehq.org/wine/source/8.x/wine-$pkgver.tar.xz
	wine-staging-$pkgver.tar.gz::https://github.com/wine-staging/wine-staging/archive/v$pkgver.tar.gz
	rpath.patch
	"
builddir="$srcdir/wine-$pkgver"
options="textrels !check" # As of 2.0.3 most of the tests fails
# ignore tracing something in a weird rpath just to silence a warning
# no effect
somask="ntdll.so win32u.so"

prepare() {
	default_prepare
	# fix opencl header detection
	sed 's|OpenCL/opencl.h|CL/opencl.h|g' -i configure*

	"$srcdir/wine-staging-$pkgver"/staging/patchinstall.py DESTDIR="$builddir" --all
}

case "$CARCH" in
x86_64)
	# also pull in 32-target, enabled with --enable-archs
	makedepends="$makedepends i686-mingw-w64-gcc"
	;;
esac

build() {
	export CFLAGS="$CFLAGS -O2 -Wno-error=format-security"
	export CXXFLAGS="$CXXFLAGS -O2 -Wno-error=format-security"
	export CPPFLAGS="$CPPFLAGS -O2 -Wno-error=format-security"

	# invalid to the msys linker
	export LDFLAGS="${LDFLAGS/,-Wl,-z,pack-relative-relocs}"

	case "$CARCH" in
	x86_64)
		local win64="--enable-win64"
		local archs="--enable-archs=x86_64,i386"
		;;
	x86)
		local no_pie="-no-pie"
		;;
	esac
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--libdir=/usr/lib \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--with-dbus \
		--with-mingw \
		--with-x \
		--with-vulkan \
		$win64 \
		$archs
	make LDFLAGS="$LDFLAGS $no_pie" tools/winedump/winedump
	make
}

check() {
	xvfb-run make test
}

package() {
	make DESTDIR="$pkgdir" install

	install -Dm755 tools/wineapploader \
		"$pkgdir"/usr/bin/wineapploader

	local file
	for file in msiexec notepad regedit regsvr32 wineboot \
		winecfg wineconsole winefile winemine winepath; do
		rm -v "$pkgdir"/usr/bin/$file
		ln -sfv /usr/bin/wineapploader "$pkgdir"/usr/bin/$file
	done

	case "$CARCH" in
	x86_64)
		x86_64-w64-mingw32-strip --strip-unneeded "$pkgdir"/usr/lib/wine/x86_64-windows/*.dll
		i686-w64-mingw32-strip --strip-unneeded "$pkgdir"/usr/lib/wine/i386-windows/*.dll

		# add compat links, since with archs these rename back to non-64
		ln -sfv wine "$pkgdir"/usr/bin/wine64
		ln -sfv wine-preloader "$pkgdir"/usr/bin/wine64-preloader
		;;
	x86)
		i686-w64-mingw32-strip --strip-unneeded "$pkgdir"/usr/lib/wine/i386-windows/*.dll
		;;
	esac
}

dev() {
	default_dev

	local file
	for file in widl wmc wrc winebuild winedump function_grep.pl \
		winedbg winemaker winegcc winecpp wineg++; do
		amove usr/bin/$file
	done
}

doc() {
	default_doc
	rm -fr "$subpkgdir"/usr/share/man/*.UTF-8
}

sha512sums="
86dffc3c9e01506ff2ff75663f48bb3b18a6afaf6381fc3c43c476481cb5c0570129550d2047059f528855e454a629c63e8beb85d5c591d1fdb7a066fbca2623  wine-8.20.tar.xz
0a107830a3589c91e8573323ce074209c52284e63561ccf3191afe03b586d93de8911b1432736069174a52dd6cbb53ebbca1c5e812b3e765a73e231df1251ccc  wine-staging-8.20.tar.gz
9881abfd0e7e4589cd5724291f9ec06ccb9d88b842c69abb5ea7865b6c27c778a060e60a9776c3620a030555cf61b8cceded55db9f04d5e45d9c627306763bff  rpath.patch
"
