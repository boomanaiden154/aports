# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=toot
pkgver=0.38.2
pkgrel=0
pkgdesc="mastodon cli & tui"
url="https://github.com/ihabunek/toot"
arch="noarch"
license="GPL-3.0-only"
depends="py3-requests py3-beautifulsoup4 py3-wcwidth py3-urwid py3-tomlkit"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/t/toot/toot-$pkgver.tar.gz"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	# Integration tests require a running Mastodon instance.
	.testenv/bin/python3 -m pytest --ignore=tests/integration/
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
d19998659d3be4a5c349e24b93dca671ae89e1cf3012c0845c0efa0355d81784fac2929f7a40ef675fa332684355f23b5245a5cc4937bd7e8aed2bb988b700aa  toot-0.38.2.tar.gz
"
