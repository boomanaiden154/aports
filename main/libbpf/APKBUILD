# Maintainer: Adam Jensen <acjensen@gmail.com>
pkgname=libbpf
pkgver=1.2.2
pkgrel=0
pkgdesc="A library for interacting with the Linux kernel's Berkeley Packet Filter (BPF) facility from user space"
url="https://github.com/libbpf/libbpf"
arch="all"
license="LGPL-2.1-only OR BSD-2-Clause"
makedepends="
	elfutils-dev
	linux-headers
	zlib-dev
	"
subpackages="$pkgname-dev"
source="https://github.com/libbpf/libbpf/archive/v$pkgver/libbpf-$pkgver.tar.gz
	10-consolidate-lib-dirs.patch
	"

build() {
	make -C src
}

check() {
	echo "#include \"$builddir/src/btf.h\"" | gcc -xc -c -o /dev/null -
}

package() {
	make -C src install DESTDIR="$pkgdir"

	# Install somewhere out of the way that will hopefully not be included by mistake.
	cd include/uapi/linux
	install -D -m644 -t "$pkgdir"/usr/include/bpf/uapi/linux/ \
		bpf.h \
		bpf_common.h \
		btf.h
}

sha512sums="
bc7620207e6f521b9b5baab00bd81346084b8eabf81bff3ec24e5367d389f2a331a0b082798f8bb5d4fea836c3c0cc961fc881abc3a4e05d91152150bdfe47be  libbpf-1.2.2.tar.gz
a374386f8b361b60c70d566aebb11b87a23bc43d8cfa02cce9997961139f89caf691d826020c4b08159e815c14d87cc907f2cc6e784329d0288d18d2b609fefd  10-consolidate-lib-dirs.patch
"
