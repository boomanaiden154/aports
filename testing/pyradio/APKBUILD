# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=pyradio
pkgver=0.9.2.19
pkgrel=0
pkgdesc="Curses based internet radio player"
url="https://www.coderholic.com/pyradio"
arch="noarch"
license="MIT"
depends="
	python3
	py3-dateutil
	py3-dnspython
	py3-netifaces
	py3-psutil
	py3-requests
	py3-rich
	"
makedepends="py3-gpep517 py3-installer py3-setuptools py3-wheel"
options="!check" # no testsuite
subpackages="$pkgname-doc $pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/coderholic/pyradio/archive/refs/tags/$pkgver.tar.gz"

prepare() {
	default_prepare

	sed -i 's/^distro = None$/distro = AlpineLinux/' pyradio/config
}

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl

	install -Dm644 devel/pyradio.png \
		-t "$pkgdir"/usr/share/icons/hicolor/512x512/apps/
	install -Dm644 devel/pyradio.desktop \
		-t "$pkgdir"/usr/share/applications/

	install -Dm644 pyradio.1 pyradio_rb.1 pyradio_rec.1 pyradio_server.1 \
		-t "$pkgdir"/usr/share/man/man1/
}

sha512sums="
1aa0e29b71749bbc26a32b404a3543643b3cbb21f7fc816da30b20950bc5b68a017b408ee6802426ed903cc097d83d87ea76e9feed8d898bddfb31e88f0834c2  pyradio-0.9.2.19.tar.gz
"
