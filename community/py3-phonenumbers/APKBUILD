# Contributor: Kaarle Ritvanen <kunkku@alpinelinux.org>
# Maintainer: Kaarle Ritvanen <kunkku@alpinelinux.org>
pkgname=py3-phonenumbers
pkgver=8.13.25
pkgrel=0
pkgdesc="International phone number library for Python"
url="https://github.com/daviddrysdale/python-phonenumbers"
arch="noarch"
license="Apache-2.0"
depends="python3"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/p/phonenumbers/phonenumbers-$pkgver.tar.gz"
builddir="$srcdir/phonenumbers-$pkgver"

replaces="py-phonenumbers" # Backwards compatibility
provides="py-phonenumbers=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 testwrapper.py
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
a3a9718adad080664c1fc61affbcb8792eafa7deaa81f03df23ff3bd0a219ee4fab1655eb74cd95a8794d53ee14d34d94b3a0a92029b0b8473dc614bc0955ca0  phonenumbers-8.13.25.tar.gz
"
